var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(opts =>
{
    var allowedCorsOrigins = builder.Configuration
        .GetSection("AllowedCorsOrigins")
        .AsEnumerable()
        .Select(x => x.Value)
        .Where(x => !string.IsNullOrEmpty(x))
        .ToArray();

    opts.WithOrigins(allowedCorsOrigins)
        .AllowCredentials()
        .AllowAnyMethod()
        .AllowAnyHeader();
});

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
